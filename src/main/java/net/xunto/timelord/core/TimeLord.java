package net.xunto.timelord.core;

import java.time.LocalTime;
import java.util.function.Supplier;

public class TimeLord {
    public static final TimeLord instance = new TimeLord(LocalTime::now);

    static final double SECONDS_PER_HOUR = 3600;
    static final double SECONDS_OFFSET = SECONDS_PER_HOUR * 6;
    static final double SECONDS_PER_DAY = 86400;
    static final double TICKS_PER_DAY = 24000;

    private final Supplier<LocalTime> nowSupplier;

    TimeLord(Supplier<LocalTime> nowSupplier) {
        this.nowSupplier = nowSupplier;
    }

    public void worldTick(IWorld world) {
        if (world.shouldSync()) {
            double offset = world.getSyncOffset();
            double seconds = this.nowSupplier.get().toSecondOfDay() + offset;

            // We need to add additional TICKS_PER_DAY to avoid ticks amount being negative.
            double ticks = TICKS_PER_DAY * (1 + (seconds - SECONDS_OFFSET) / SECONDS_PER_DAY);
            // Clamp ticks to the first day.
            ticks %= TICKS_PER_DAY;

            world.disableDefaultTimeCycle();
            world.setTime((long) ticks);
        }
    }
}
