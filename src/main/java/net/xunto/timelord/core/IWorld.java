package net.xunto.timelord.core;

public interface IWorld {
    void setTime(long tick);

    boolean shouldSync();

    int getSyncOffset();

    void disableDefaultTimeCycle();
}
