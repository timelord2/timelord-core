package net.xunto.timelord.core;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.time.LocalTime;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

public class TimeLordTest {

    static Stream<Arguments> tickToTimeParams() {
        return Stream.of(
                Arguments.of(LocalTime.of(6, 0), 0),
                Arguments.of(LocalTime.of(12, 0), 6000),
                Arguments.of(LocalTime.of(18, 0), 12000),
                Arguments.of(LocalTime.of(0, 0), 18000),
                Arguments.of(LocalTime.of(5, 59, 31), 23991)
        );
    }

    static Stream<Arguments> tickToTimeParamsWithOffset() {
        return Stream.of(
                Arguments.of(LocalTime.of(6, 0), 21600, 6000),
                Arguments.of(LocalTime.of(5, 59, 31), 21600, 5991)
        );
    }

    @ParameterizedTest
    @MethodSource("tickToTimeParams")
    public void testSync(LocalTime time, int tick) {
        Supplier<LocalTime> now = Mockito.mock(Supplier.class);
        doReturn(time).when(now).get();
        IWorld world = mock(IWorld.class);
        doReturn(true).when(world).shouldSync();
        doReturn(0).when(world).getSyncOffset();

        new TimeLord(now).worldTick(world);

        verify(world).setTime(tick);
        verify(world).disableDefaultTimeCycle();
        verify(world).getSyncOffset();
        verify(world).shouldSync();
    }

    @ParameterizedTest
    @MethodSource("tickToTimeParamsWithOffset")
    public void testOffset(LocalTime time, int offset, int tick) {
        Supplier<LocalTime> now = Mockito.mock(Supplier.class);
        doReturn(time).when(now).get();
        IWorld world = mock(IWorld.class);
        doReturn(true).when(world).shouldSync();
        doReturn(offset).when(world).getSyncOffset();

        new TimeLord(now).worldTick(world);

        verify(world).setTime(tick);
        verify(world).disableDefaultTimeCycle();
        verify(world).getSyncOffset();
        verify(world).shouldSync();
    }

    @Test
    public void testSyncDisabled() {
        Supplier<LocalTime> now = mock(Supplier.class);
        IWorld world = mock(IWorld.class);
        doReturn(false).when(world).shouldSync();

        new TimeLord(now).worldTick(world);

        verify(world).shouldSync();
        verify(world, never()).setTime(any(Long.class));
        verify(world, never()).disableDefaultTimeCycle();
        verify(world, never()).getSyncOffset();
    }
}
